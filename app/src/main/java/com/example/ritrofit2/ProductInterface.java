package com.example.ritrofit2;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ProductInterface {
    public static final String BASE_URL = "http://10.0.2.2//android/";
    @GET("get_all_products.php")
    Call<Products> getProducts();

}
